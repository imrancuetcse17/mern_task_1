import "./App.css";
import Navbar from "./components/Navbar";
import { Switch, Route, Redirect} from "react-router-dom";
import Upload from "./components/UploadPage/Upload";
import ImageList from "./components/ImageList";

function App() {
  return (
    <div className="App">
      <Navbar></Navbar>
      <Switch>
        <Route exact path="/upload" component={Upload}></Route>
        <Route exact path="/image-list" component={ImageList}></Route>
        <Redirect to="/upload"></Redirect>
      </Switch>
    </div>
  );
}

export default App;
