import axios from "axios";
import React, { useEffect, useState } from "react";
import "./ImageList.css";

export default function ImageList() {
  const [data, setData] = useState(null);

  useEffect(() => {
    axios
      .get("http://127.0.0.1:5000/api/get-image-list")
      .then((res) => {
        setData(res.data);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <>
      {data != null ? (
        <div className={"image-list-container"}>
          {data.map((tile) => (
            <img src={tile.imageData} style={{backgroundColor:'gray',padding:"10px"}} alt={"could'nt load"} />
          ))}
        </div>
      ) : (
        <>
        <h1 style={{textAlign:'center'}}>Loading...</h1>
        </>
      )}
    </>
  );
}
