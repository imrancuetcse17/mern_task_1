import React, { useEffect, useState } from "react";
import Webcam from "react-webcam";
import Button from "@material-ui/core/Button";
import { OpenCvProvider, useOpenCv } from "opencv-react";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import "./Upload.css";

const useStyles = makeStyles((theme) => ({
  button: {
    backgroundColor: "#3c52b2",
    color: "#fff",
    marginTop: "50px",
    "&:hover": {
      backgroundColor: "#3c52b2",
      color: "#fff",
    },
  },
}));

function UploadComponent() {
  const webcamRef = React.useRef(null);
  const classes = useStyles();
  const { cv } = useOpenCv();
  const [image, setImage] = useState(null);
  const [process, setProcess] = useState(false);

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImage(imageSrc);
    setProcess(true);
  }, [webcamRef]);

  const handleClose = React.useCallback(() => {
    setImage(null);
  });

  useEffect(() => {
    if (process) {
      let imageElement = document.createElement("img");
      imageElement.src = image;

      let mat = cv.imread(imageElement);
      let p1 = new cv.Point(Math.floor(Math.random() * 640), Math.floor(Math.random() * 480));
      let p2 = new cv.Point(Math.floor(Math.random() * 640), Math.floor(Math.random() * 480));
      let p3 = new cv.Point(Math.floor(Math.random() * 640), Math.floor(Math.random() * 480));
      let p4 = new cv.Point(Math.floor(Math.random() * 640), Math.floor(Math.random() * 480));

      cv.rectangle(mat, p1, p2, [0, 0, 0, 255], 2);
      cv.rectangle(mat, p3, p4, [0, 0, 0, 255], 2);

      let canvas = document.createElement("canvas");
      cv.imshow(canvas, mat);

      setImage(canvas.toDataURL());
      axios
        .post("http://127.0.0.1:5000/api/upload-image", {
          imageData: canvas.toDataURL(),
        })
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
      setProcess(false);
    }
    if (cv) {
    }
  }, [cv, process]);
  return (
    <div className={"upload-div"}>
      {image != null ? (
        <div className="modal-container">
          <div className={"header"}>
            <h1>Result</h1>
            <button onClick={handleClose}>
              <CloseIcon />
            </button>
          </div>
          <div className={"result-image-container"}>
            <img src={image} alt={"halum"}></img>
          </div>
        </div>
      ) : (
        <>
          <div className={"camera-container"}>
            <Webcam ref={webcamRef} screenshotFormat="image/jpeg"></Webcam>
          </div>
          <Button className={classes.button} onClick={capture}>
            Capture & Draw Box
          </Button>
        </>
      )}
    </div>
  );
}

const Upload = () => {
  return (
    <OpenCvProvider>
      <UploadComponent />
    </OpenCvProvider>
  );
};

export default Upload;
