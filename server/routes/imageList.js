const express = require('express');

const controller = require('../controllers/imageList');

const router = express.Router();

router.route('/upload-image').post(controller.uploadImage);
router.route('/get-image-list').get(controller.getImageList);

module.exports = router;
