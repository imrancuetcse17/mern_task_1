/* eslint-disable no-prototype-builtins */
/* eslint-disable no-unused-vars */
const ImageListSchema = require("../models/imageList");

module.exports = {
  uploadImage: (req, res, next) => {
    const { imageData } = req.body;

    const imageList = new ImageListSchema({
      imageData,
    });

    imageList
      .save()
      .then((result) => {
        res.send(result);
      })
      .catch((err) => {
        res.send(err);
      });
  },
  getImageList: (req, res, next) => {
    ImageListSchema.find({})
      .then((result) => {
        res.send(result);
      })
      .catch((err) => {
        res.send(err);
      });
  },
};
