const mongoose = require("mongoose");

const { Schema } = mongoose;

const ImageListSchema = new Schema({
  imageData: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("imageList", ImageListSchema);
