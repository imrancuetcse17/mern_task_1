const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const bodyParser = require("body-parser");
const imageListRouter = require("./routes/imageList");
const app = express();

app.use(cors());
var jsonParser = bodyParser.json({limit:1024*1024*10, type:'application/json'}); 
var urlencodedParser = bodyParser.urlencoded({ extended:true,limit:1024*1024*10,type:'application/x-www-form-urlencoded' });
app.use(jsonParser);
app.use(urlencodedParser);

app.use("/api", imageListRouter);
app.use("/", (req, res) => {
  res.send("Api is Running");
});

mongoose
  .connect(process.env.mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("database connected");
  })
  .catch(() => {
    throw err;
  });

app.listen(process.env.PORT | 5000, () => {
  console.log("server running");
});
